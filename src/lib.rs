#[cfg(test)]
mod test;

use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::slice::IterMut;

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct Grade {
    mark: f64,
    weight: f64,
}

impl Grade {
    pub fn new(mark: f64, weight: f64) -> Grade {
        Grade {
            mark: mark.clamp(0.0, 1.0),     // In percentage
            weight: weight.clamp(0.0, 1.0), // In percentage
        }
    }
}

impl fmt::Display for Grade {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Mark (%): {}, Weight (%): {}",
            self.mark * 100.0,
            self.weight * 100.0,
        )
    }
}

#[derive(Debug, PartialOrd, Clone)]
pub struct Assessment {
    pub name: String,
    pub description: String,
    grade: Option<Grade>,
}

impl PartialEq for Assessment {
    fn eq(&self, other: &Assessment) -> bool {
        self.name == other.name
    }
}

impl fmt::Display for Assessment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}: {} - Grade: {}",
            self.name,
            self.description,
            self.grade
                .clone()
                .map_or("None".to_owned(), |x| x.to_string())
        )
    }
}

impl Default for Assessment {
    fn default() -> Self {
        Assessment { name: "Unnamed Assessment".to_owned(), description: "Empty Assessment Description".to_owned(), grade: None }
    }
}

impl Eq for Assessment {}

impl Hash for Assessment {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.name.hash(hasher)
    }
}

impl Assessment {
    pub fn new(name: &str, description: &str, grade: Option<Grade>) -> Assessment {
        Assessment {
            name: name.to_owned(),
            description: description.to_owned(),
            grade,
        }
    }
    pub fn set_grade(&mut self, grade: Grade) {
        self.grade = Some(grade);
    }
}

#[derive(Clone, Debug)]
pub struct Subject {
    pub code: String,
    pub name: String,
    pub description: String,
    assessments: HashMap<u64, Assessment>,
}

impl PartialEq for Subject {
    fn eq(&self, other: &Subject) -> bool {
        self.code == other.code
    }
}

impl Hash for Subject {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.code.hash(hasher)
    }
}

impl Default for Subject {
    fn default() -> Self {
        Subject { code: "Empty Subject Code".to_owned(), name: "Unnamed Subject".to_owned(), description: "Empty Subject Description".to_owned(), assessments: HashMap::new() }
    }
}

impl fmt::Display for Subject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} - {}:\n{}\nAssessments:\n{}",
            self.code,
            self.name,
            self.description,
            match self.assessments.clone().into_iter().fold(
                None,
                |prev: Option<String>, kv: (u64, Assessment)| {
                    Some(prev.map_or("".to_owned(), |val| val + ", ") + &kv.1.to_string())
                }
            ) {
                Some(s) => "{\n".to_owned() + &s + "\n}",
                None => "{No Assessments}".to_owned(),
            }
        )
    }
}

impl IntoIterator for Subject {
    type Item = Assessment;
    type IntoIter = std::collections::hash_set::IntoIter<Assessment>;

    fn into_iter(self) -> Self::IntoIter {
        self.assessments
            .values()
            .map(|m| m.to_owned())
            .collect::<HashSet<Assessment>>()
            .into_iter()
    }
}

impl Subject {
    pub fn new(code: &str, name: &str, description: &str) -> Subject {
        Subject {
            code: code.to_owned(),
            name: name.to_owned(),
            description: description.to_owned(),
            assessments: HashMap::new(),
        }
    }

    pub fn add_assessment(&mut self, ass: Assessment) -> u64 {
        let key = self.assessments.len() as u64;
        self.assessments.insert(key, ass.clone());
        key
    }

    pub fn get_assessment(&mut self, key: u64) -> Option<&mut Assessment> {
        self.assessments.get_mut(&key)
    }

    pub fn total_grade(&self) -> f64 {
        round_sf(
            self.assessments
                .iter()
                .fold(0.0, |prev: f64, kv: (&u64, &Assessment)| {
                    prev + kv.1.grade.clone().map_or(0.0, |x| x.mark)
                        * kv.1.grade.clone().map_or(0.0, |x| x.weight)
                }),
            4,
        )
    }

    pub fn average_grade(&self) -> f64 {
        round_sf(
            self.assessments
                .iter()
                .fold(0.0, |prev: f64, kv: (&u64, &Assessment)| {
                    prev + kv.1.grade.clone().map_or(0.0, |x| x.mark)
                })
                / (self.assessments.len() as f64),
            4,
        )
    }
}

#[derive(Clone)]
pub struct Semester {
    pub name: String,
    pub subjects: [Option<Subject>; 4],
}

impl Default for Semester {
    fn default() -> Self {
        Semester { name: "Unnamed Semester".to_owned(), subjects: [None, None, None, None] }
    }
}

impl fmt::Display for Semester {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}:\nSubject 1:\n{}\n\nSubject 2:\n{}\n\nSubject 3:\n{}\n\nSubject 4:\n{}",
            self.name,
            self.subjects[0]
                .as_ref()
                .map_or("None".to_owned(), |x| x.to_string()),
            self.subjects[1]
                .as_ref()
                .map_or("None".to_owned(), |x| x.to_string()),
            self.subjects[2]
                .as_ref()
                .map_or("None".to_owned(), |x| x.to_string()),
            self.subjects[3]
                .as_ref()
                .map_or("None".to_owned(), |x| x.to_string())
        )
    }
}

impl IntoIterator for Semester {
    type Item = Option<Subject>;
    type IntoIter = std::array::IntoIter<Option<Subject>, 4>;

    fn into_iter(self) -> Self::IntoIter {
        self.subjects.into_iter()
    }
}

impl Semester {
    pub fn new(name: &str) -> Self {
        Semester {
            name: name.to_string(),
            subjects: [None, None, None, None],
        }
    }

    pub fn insert_subject(&mut self, sub: Subject, slot: usize) -> Option<Subject> {
        let ret = self.subjects.clone().get(slot).map_or(None, |x| x.to_owned());
        match self.subjects.get_mut(slot.clamp(0, 3)) {
            Some(subject_ref) => {
                *subject_ref = Some(sub);
            }
            None => {}
        }
        ret
    }

    pub fn add_subject(&mut self, sub: Subject) -> bool {
        for s in self.subjects.iter_mut() {
            match s {
                Some(_) => (),
                None => {
                    *s = Some(sub.clone());
                    return true;
                }
            }
        }
        false
    }

    pub fn get_subject(&self, slot: usize) -> Option<&Subject> {
        self.subjects[slot].as_ref()
    }

    pub fn get_mut_subject(&mut self, slot: usize) -> Option<&mut Subject> {
        self.subjects[slot].as_mut()
    }

    pub fn into_mut_iter(&mut self) -> IterMut<Option<Subject>> {
        self.subjects.iter_mut()
    }
}

#[derive(Clone)]
pub struct Folio {
    pub name: String,
    semesters: HashMap<u64, Semester>,
}

impl Default for Folio {
    fn default() -> Self {
        Folio {
            name: "Unnamed Folio".to_owned(),
            semesters: HashMap::new()
        }
    }
}

impl fmt::Display for Folio {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}:\n{}",
            self.name,
            match self.clone().into_iter().fold(
                None,
                |prev: Option<String>, kv: (u64, Semester)| {
                    Some(prev.map_or("".to_owned(), |s| s + "\n") + &kv.1.to_string())
                }
            ) {
                Some(str) => str,
                None => "EMPTY FOLIO".to_owned(),
            }
        )
    }
}

impl IntoIterator for Folio {
    type Item = (u64, Semester);
    type IntoIter = std::collections::hash_map::IntoIter<u64, Semester>;

    fn into_iter(self) -> Self::IntoIter {
        self.semesters.into_iter()
    }
}

fn round_sf(x: f64, n: i32) -> f64 {
    let exp: f64 = 10.0_f64.powi(n);
    (x * exp).round() / exp
}

impl Folio {
    pub fn new(name: &str) -> Folio {
        Folio {
            name: name.to_owned(),
            semesters: HashMap::new(),
        }
    }

    pub fn add_semester(&mut self, sem: Semester) -> u64 {
        let key = self.semesters.len() as u64;
        self.semesters.insert(key, sem);
        key
    }

    pub fn insert_semester(&mut self, sem: Semester, key: &u64) -> Option<Semester> {
        self.semesters.insert(key.to_owned(), sem)
    }

    pub fn remove_semester(&mut self, key: &u64) -> Option<Semester> {
        self.semesters.remove(key)
    }

    pub fn get_semester(&mut self, key: &u64) -> Option<&mut Semester> {
        self.semesters.get_mut(key)
    }

    pub fn contains(&self, name: &str) -> bool {
        self.semesters.values().any(|x| x.name.eq(name))
    }
}
