use grader::{Folio, Semester, Subject, Assessment};

use eframe::egui;

fn main() {
    let options = eframe::NativeOptions::default();
    eframe::run_native(
        "Grader",
        options,
        Box::new(|_cc| Box::new(GraderApp::default())),
    );
}

struct GraderApp {
    folio: Folio,
    /*selected_semester: Option<Semester>,
    selected_subject: Option<Subject>,
    selected_slot: Option<usize>,
    selected_assessment: Option<Assessment>,*/
}

impl Default for GraderApp {
    fn default() -> Self {
        Self {
            folio: Folio::default(),
            /*
            selected_semester: None,
            selected_subject: None,
            selected_slot: None,
            selected_assessment: None
            */
        }
    }
}

impl eframe::App for GraderApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let mut selected_semester: Option<(u64, &mut Semester)> = None;
        let mut selected_subject: Option<(usize, &mut Subject)> = None;
        let mut selected_assessment: Option<(u64, &mut Assessment)> = None;

        egui::SidePanel::left("side_panel_1").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.label("Folio Name: ");
                ui.text_edit_singleline(&mut self.folio.name);
            });
            egui::Frame::none().fill(egui::Color32::LIGHT_GRAY).show(ui, |ui| {
                ui.label("Semesters:");
                egui::ScrollArea::vertical().show(ui, |ui| {
                    for (key, sem)in self.folio.clone().into_iter() {
                        if ui.button(&sem.name.clone()).clicked() {
                            selected_semester = self.folio.get_semester(&key).map(|x| (key, x));
                        }
                    }
                });
            });
            if ui.button("Create Semester").clicked() {
                let key = self.folio.add_semester(Semester::default());
                selected_semester = self.folio.get_semester(&key).map(|x| (key, x));
            }
        });
        egui::SidePanel::right("side_panel_2").default_width(300.0).show(ctx, |ui| {
            if selected_semester.is_none() {
                return;
            }
            match selected_subject {
                Some((_, sub)) => {
                    ui.horizontal(|ui| {
                        ui.label("Subject Name: ");
                        ui.text_edit_singleline(&mut sub.name);
                    });
                    if ui.button("Create Assessment").clicked() {
                        let key = sub.add_assessment(Assessment::default());
                        selected_assessment = sub.get_assessment(key).map(|x| (key, x));
                    }
                }
                None => {}
            }
        });
        if selected_assessment.is_some() {
            egui::Window::new("New Assessment").show(ctx, |ui| {
                ui.label("text");
                if ui.button("Close").clicked() {
                    selected_assessment = None;
                }
            });
        }
        egui::CentralPanel::default().show(ctx, |ui| {
            // The central pane will show the currently selected semester, essentially listing all subjects
            match selected_semester {
                Some((key, sem)) => {
                    ui.horizontal(|ui| {
                        ui.label("Semester Name: ");
                        ui.text_edit_singleline(&mut sem.name);
                    });
                    if ui.button(if self.folio.contains(&sem.name) {
                            "Modify"
                        } else {
                            "Add"
                        }).clicked() {
                            self.folio.add_semester(sem.clone());
                    }
                    if self.folio.contains(&sem.name) && ui.button("Delete").clicked() {
                        self.folio.remove_semester(&key);
                        selected_subject = None;
                        selected_assessment = None;
                    }
                    egui::Frame::none().fill(egui::Color32::LIGHT_GRAY).show(ui, |ui| {
                        ui.label("Subjects:");
                        for (slot, _) in sem.clone().into_iter().enumerate() {
                            match sem.get_subject(slot) {
                                Some(subject) => {
                                    if ui.button(&subject.name).clicked() {
                                        selected_subject = sem.get_mut_subject(slot).map(|x| (slot, x));
                                    }
                                }
                                None => {
                                    if ui.button("Empty Slot").clicked() {
                                        sem.insert_subject(Subject::default(), slot);
                                        selected_subject = sem.get_mut_subject(slot).map(|x| (slot, x));
                                    }
                                }
                            }
                        }
                    });
                }
                None => {}
            }
        });
    }
}
