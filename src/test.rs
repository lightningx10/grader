use super::*;

#[test]
fn subject_equality_test() {
    let s1 = Subject::new(
        "COMP2300",
        "Computer Organisation & Program Execution",
        "Learning systems architecture, programming in ARM assembly",
    );
    let s2 = Subject::new(
        "COMP2300",
        "Computer Organisation & Program Execution",
        "Learning systems architecture, programming in ARM assembly",
    );
    assert_eq!(s1, s2);

    let s3 = Subject::new(
        "COMP2310",
        "Bobs burger flipping",
        "Wait we're meant to be learning?",
    );
    assert_ne!(s1, s3);
    assert_ne!(s2, s3);

    let s4 = Subject::new("COMP2300", "Typo here", "Another typo here, woops");
    assert_eq!(s1, s4);
    assert_eq!(s2, s4);
    assert_ne!(s3, s4);
}

#[test]
fn subject_grade_test() {
    let mut s1 = Subject::new(
        "COMP2300",
        "Computer Organisation & Program Execution",
        "Learning systems architecture, programming in ARM assembly",
    );
    s1.add_assessment(Assessment::new(
        "Ass1",
        "Description",
        Some(Grade::new(0.7, 0.5)),
    ));
    s1.add_assessment(Assessment::new(
        "Ass2",
        "Description",
        Some(Grade::new(0.6, 0.5)),
    ));
    assert_eq!(s1.total_grade(), 0.65);
    assert_eq!(s1.average_grade(), 0.65);
    let mut s2 = Subject::new(
        "COMP2300",
        "Description",
        "Learning systems architecture, programming in ARM assembly",
    );
    s2.add_assessment(Assessment::new(
        "Assessment 1 Presubmission",
        "Description",
        Some(Grade::new(0.75, 0.01)),
    ));
    s2.add_assessment(Assessment::new(
        "Assessment 1",
        "Description",
        Some(Grade::new(18.0 / 20.0, 0.2)),
    ));
    s2.add_assessment(Assessment::new(
        "Assessment 2 Presubmission",
        "Description",
        Some(Grade::new(0.75, 0.01)),
    ));
    s2.add_assessment(Assessment::new(
        "Assessment 2",
        "Description",
        Some(Grade::new(19.0 / 20.0, 0.2)),
    ));
    s2.add_assessment(Assessment::new(
        "Lab Pack 1",
        "Description",
        Some(Grade::new(1.0, 0.015)),
    ));
    s2.add_assessment(Assessment::new(
        "Lab Pack 2",
        "Description",
        Some(Grade::new(1.0, 0.015)),
    ));
    s2.add_assessment(Assessment::new(
        "Lab Pack 3",
        "Description",
        Some(Grade::new(1.0, 0.015)),
    ));
    s2.add_assessment(Assessment::new(
        "Lab Pack 4",
        "Description",
        Some(Grade::new(1.0, 0.015)),
    ));
    s2.add_assessment(Assessment::new(
        "Lab Attendence",
        "Description",
        Some(Grade::new(3.5 / 4.0, 0.04)),
    ));
    s2.add_assessment(Assessment::new(
        "Mid-Semester Exam",
        "Description",
        Some(Grade::new(14.8 / 20.0, 0.2)),
    ));
    s2.add_assessment(Assessment::new(
        "Final Exam",
        "Description",
        Some(Grade::new(25.75 / 30.0, 0.3)),
    ));
    s2.add_assessment(Assessment::new(
        "Quiz 1",
        "Description",
        Some(Grade::new(1.0, 0.01)),
    ));
    s2.add_assessment(Assessment::new(
        "Quiz 2",
        "Description",
        Some(Grade::new(1.0, 0.01)),
    ));
    assert_eq!(s2.average_grade(), 0.9095);
    assert_eq!(s2.total_grade(), 0.9055);
}
